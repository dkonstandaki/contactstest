package com.dk.contactstest.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.j256.ormlite.field.DatabaseField;

public class Contact implements Parcelable, Comparable<Contact> {

    @DatabaseField(generatedId = true)
    public int id;
    @DatabaseField
    public String forename;
    @DatabaseField
    public String surname;
    @DatabaseField
    public String patronymic;
    @DatabaseField
    public String telephone;
    @DatabaseField
    public String badge;

    public Contact() {
        // needed by ormlite
    }

    private Contact(Parcel in) {
        id = in.readInt();
        forename = in.readString();
        surname = in.readString();
        patronymic = in.readString();
        telephone = in.readString();
        badge = in.readString();
    }

    public void writeToParcel(Parcel out, int flags) {
        out.writeInt(id);
        out.writeString(forename);
        out.writeString(surname);
        out.writeString(patronymic);
        out.writeString(telephone);
        out.writeString(badge);
    }

    public static final Parcelable.Creator<Contact> CREATOR = new Parcelable.Creator<Contact>() {
        public Contact createFromParcel(Parcel in) {
            return new Contact(in);
        }
        public Contact[] newArray(int size) {
            return new Contact[size];
        }
    };

    public int describeContents() {
        return 0;
    }

    @Override
    public int compareTo(Contact other) {
        if (this.surname.equals(other.surname))
            return this.forename.compareTo(other.forename);
        else if (this.forename.equals(other.forename))
            return this.patronymic.compareTo(other.patronymic);
        else
            return this.surname.compareTo(other.surname);
    }
}
