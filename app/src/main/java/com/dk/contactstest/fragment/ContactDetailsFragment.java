package com.dk.contactstest.fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RadioGroup;

import com.dk.contactstest.R;
import com.dk.contactstest.db.DatabaseHelper;
import com.dk.contactstest.model.Contact;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.RuntimeExceptionDao;

public class ContactDetailsFragment extends Fragment implements RadioGroup.OnCheckedChangeListener {

    private DatabaseHelper databaseHelper = null;
    private RuntimeExceptionDao<Contact, Integer> contactDao;

    private Contact contact;

    private EditText etSurname;
    private EditText etForename;
    private EditText etPatronymic;
    private EditText etTelephone;
    private RadioGroup rgBadges;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        contactDao = getDatabaseHelper().getPoiDao();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().onBackPressed();
            }
        });

        View v = inflater.inflate(R.layout.fragment_details, container, false);
        etSurname = (EditText) v.findViewById(R.id.etContactSurname);
        etForename = (EditText) v.findViewById(R.id.etContactForename);
        etPatronymic = (EditText) v.findViewById(R.id.etContactPatronymic);
        etTelephone = (EditText) v.findViewById(R.id.etContactTelephone);
        rgBadges = (RadioGroup) v.findViewById(R.id.rgBadges);
        rgBadges.setOnCheckedChangeListener(this);
        setupContactData(toolbar);
        return v;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_details, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.menu_done) {
            saveContact();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCheckedChanged(RadioGroup radioGroup, int id) {
        contact.badge = getActivity().getResources().getResourceEntryName(id);
    }

    private void setupContactData(Toolbar toolbar) {
        Bundle args = getArguments();
        if (args != null && args.containsKey(ContactsFragment.KEY_SELECTED_CONTACT)) {
            contact = args.getParcelable(ContactsFragment.KEY_SELECTED_CONTACT);
            toolbar.setTitle(R.string.edit_contact);
            etSurname.setText(contact.surname);
            etForename.setText(contact.forename);
            etPatronymic.setText(contact.patronymic);
            etTelephone.setText(contact.telephone);
            rgBadges.check(getResources().getIdentifier(contact.badge, "id", getActivity().getPackageName()));
        } else {
            contact = new Contact();
            toolbar.setTitle(R.string.new_contact);
            rgBadges.check(R.id.ic_face);
        }
    }

    private void saveContact() {
        if (performValidation()) {
            contact.forename = etForename.getText().toString();
            contact.surname = etSurname.getText().toString();
            contact.patronymic = etPatronymic.getText().toString();
            contact.telephone = etTelephone.getText().toString();
            contactDao.createOrUpdate(contact);
            getActivity().onBackPressed();
        }
    }

    private boolean performValidation() {
        if (TextUtils.isEmpty(etSurname.getText())
                || TextUtils.isEmpty(etForename.getText())
                || TextUtils.isEmpty(etPatronymic.getText())
                || TextUtils.isEmpty(etTelephone.getText())) {
            Snackbar.make(getActivity().findViewById(R.id.flContainer), R.string.not_valid_msg, Snackbar.LENGTH_SHORT)
                    .show();
            return false;
        } else
            return true;
    }

    private DatabaseHelper getDatabaseHelper() {
        if (databaseHelper == null)
            databaseHelper = new DatabaseHelper(getActivity().getApplicationContext());
        return databaseHelper;
    }

    @Override
    public void onDestroy() {
        if (databaseHelper != null) {
            OpenHelperManager.releaseHelper();
            databaseHelper = null;
        }
        super.onDestroy();
    }
}
