package com.dk.contactstest.fragment;

import android.app.Fragment;
import android.app.FragmentManager;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.dk.contactstest.R;
import com.dk.contactstest.db.DatabaseHelper;
import com.dk.contactstest.model.Contact;
import com.dk.contactstest.ui.ItemTouchHelperCallback;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.RuntimeExceptionDao;

import java.util.ArrayList;
import java.util.Collections;

public class ContactsFragment extends Fragment implements FragmentManager.OnBackStackChangedListener {

    public static final String KEY_SELECTED_CONTACT = "selected_contact";

    private DatabaseHelper databaseHelper = null;
    private RuntimeExceptionDao<Contact, Integer> contactDao;

    private FragmentManager fragmentManager;
    private RecyclerView rvContacts;
    private ArrayList<Contact> contacts;
    private ContactsAdapter mAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        contactDao = getDatabaseHelper().getPoiDao();
        fragmentManager = getFragmentManager();
        fragmentManager.addOnBackStackChangedListener(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_contacts, container, false);

        rvContacts = (RecyclerView) v.findViewById(R.id.rvContacts);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        rvContacts.setLayoutManager(layoutManager);
        loadContacts();

        v.findViewById(R.id.fabAdd).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showNextFragment(new ContactDetailsFragment());
            }
        });
        return v;
    }

    @Override
    public void onBackStackChanged() {
        if (fragmentManager.getBackStackEntryCount() == 0) {
            loadContacts();
            Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
            toolbar.setNavigationIcon(null);
            toolbar.setTitle(R.string.app_name);
        }
    }

    private void loadContacts() {
        contacts = new ArrayList<>(contactDao.queryForAll());
        Collections.sort(contacts);
        mAdapter = new ContactsAdapter(contacts, new ContactsAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(Contact contact) {
                ContactDetailsFragment detailsFragment = new ContactDetailsFragment();
                Bundle args = new Bundle();
                args.putParcelable(KEY_SELECTED_CONTACT, contact);
                detailsFragment.setArguments(args);
                showNextFragment(detailsFragment);
            }
        }, new ContactsAdapter.OnItemRemoveListener() {
            @Override
            public void onItemRemove(int index) {
                contactDao.delete(contacts.get(index));
                contacts.clear();
                contacts.addAll(contactDao.queryForAll());
                Collections.sort(contacts);
                mAdapter.notifyDataSetChanged();
            }
        });
        ItemTouchHelper.Callback callback = new ItemTouchHelperCallback(mAdapter);
        ItemTouchHelper touchHelper = new ItemTouchHelper(callback);
        touchHelper.attachToRecyclerView(rvContacts);
        rvContacts.setAdapter(mAdapter);
    }

    private void showNextFragment(Fragment fragment) {
        fragmentManager.beginTransaction()
                .setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out,
                        android.R.animator.fade_in, android.R.animator.fade_out)
                .add(R.id.flContainer, fragment)
                .addToBackStack(null)
                .commit();
    }

    private DatabaseHelper getDatabaseHelper() {
        if (databaseHelper == null)
            databaseHelper = new DatabaseHelper(getActivity().getApplicationContext());
        return databaseHelper;
    }

    @Override
    public void onDestroy() {
        if (databaseHelper != null) {
            OpenHelperManager.releaseHelper();
            databaseHelper = null;
        }
        super.onDestroy();
    }
}
