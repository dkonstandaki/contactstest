package com.dk.contactstest.fragment;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.dk.contactstest.R;
import com.dk.contactstest.model.Contact;
import com.dk.contactstest.ui.ItemTouchHelperAdapter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ContactsAdapter extends RecyclerView.Adapter<ContactsAdapter.ViewHolder> implements ItemTouchHelperAdapter {

    public interface OnItemClickListener {
        void onItemClick(Contact contact);
    }

    public interface OnItemRemoveListener {
        void onItemRemove(int idx);
    }

    private List<Contact> mContacts;
    private OnItemClickListener mClickListener;
    private OnItemRemoveListener mRemoveListener;

    public ContactsAdapter(ArrayList<Contact> contacts, OnItemClickListener clickListener, OnItemRemoveListener removeListener) {
        mContacts = contacts;
        mClickListener = clickListener;
        mRemoveListener = removeListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.contacts_list_row, viewGroup, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {
        viewHolder.bind(mContacts.get(position), mClickListener);
    }

    @Override
    public int getItemCount() {
        return mContacts.size();
    }

    @Override
    public void onItemDismiss(int position) {
        mRemoveListener.onItemRemove(position);
    }

    @Override
    public void onItemMove(int fromPosition, int toPosition) {
        if (fromPosition < toPosition) {
            for (int i = fromPosition; i < toPosition; i++)
                Collections.swap(mContacts, i, i + 1);
        } else {
            for (int i = fromPosition; i > toPosition; i--)
                Collections.swap(mContacts, i, i - 1);
        }
        notifyItemMoved(fromPosition, toPosition);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        private final TextView tvNumber;
        private final ImageView ivBadge;
        private final TextView tvName;

        public ViewHolder(View v) {
            super(v);
            tvNumber = (TextView) v.findViewById(R.id.tvNumber);
            ivBadge = (ImageView) v.findViewById(R.id.ivBadge);
            tvName = (TextView) v.findViewById(R.id.tvName);
        }

        public void bind(final Contact contact, final OnItemClickListener clickListener) {
            Context context = itemView.getContext();
            int badgeId = context.getResources().getIdentifier(contact.badge, "drawable", context.getPackageName());
            String fullName = contact.surname + " " + contact.forename + " " + contact.patronymic;
            tvNumber.setText(String.valueOf(getLayoutPosition() + 1));
            ivBadge.setImageResource(badgeId);
            tvName.setText(fullName);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override public void onClick(View v) {
                    clickListener.onItemClick(contact);
                }
            });
        }
    }
}
